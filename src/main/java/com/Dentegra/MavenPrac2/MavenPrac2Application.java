package com.Dentegra.MavenPrac2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MavenPrac2Application {

	public static void main(String[] args) {
		SpringApplication.run(MavenPrac2Application.class, args);
	}
}
